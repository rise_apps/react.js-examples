import * as constants from '@app/constants';
import { ICurrency } from '@app/types/currencies';

export interface IGetListCurrenciesAction {
  type: constants.GET_LIST_OF_CURRENCIES;
  payload: {
    type: string;
  };
}

export interface IGetListCurrenciesFailAction {
  type: constants.GET_LIST_OF_CURRENCIES_FAIL;
  payload: {
    error: string;
    type: string;
  };
}

export interface IGetListCurrenciesSuccessAction {
  type: constants.GET_LIST_OF_CURRENCIES_SUCCESS;
  payload: {
    type: string;
    data: Array<ICurrency>;
  };
}

export function getListCurrencies(type: string): IGetListCurrenciesAction {
  return {
    type: constants.GET_LIST_OF_CURRENCIES,
    payload: {
      type,
    },
  };
}

export type CurrenciesAction =
  | IGetListCurrenciesAction
  | IGetListCurrenciesFailAction
  | IGetListCurrenciesSuccessAction;
