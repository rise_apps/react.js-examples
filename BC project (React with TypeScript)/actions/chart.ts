import * as constants from '@app/constants';

export interface IGetChartDataAction {
    type: constants.GET_CHART_DATA;
    payload: {
        productId: string,
        period: string
    }
}

export interface IGetChartDataStartAction {
    type: constants.GET_CHART_DATA_START;
}

export interface IGetChartDataSuccessAction {
    type: constants.GET_CHART_DATA_SUCCESS;
    payload: {
        data: [];
    };
}

export interface IGetChartDataFailAction {
    type: constants.GET_CHART_DATA_FAIL;
    payload: {
        error: string;
    };
}

export interface IGetListProductsAction {
    type: constants.GET_LIST_PRODUCTS;
}

export interface IGetListProductsStartAction {
    type: constants.GET_LIST_PRODUCTS_START;
}

export interface IGetListProductsSuccessAction {
    type: constants.GET_LIST_PRODUCTS_SUCCESS;
    payload: {
        products: [];
    };
}

export function getChartData(productId:string, period:string): IGetChartDataAction {
    return {
        type: constants.GET_CHART_DATA,
        payload:{
            productId,
            period
        }
    };
}

export function getListProducts(): IGetListProductsAction {
    return {
        type: constants.GET_LIST_PRODUCTS,
    };
}

export type ChartAction =
    | IGetChartDataAction
    | IGetChartDataStartAction
    | IGetChartDataSuccessAction
    | IGetChartDataFailAction
    | IGetListProductsAction
    | IGetListProductsStartAction
    | IGetListProductsSuccessAction;
