import * as constants from '@app/constants';
import { ItemExchange } from '@app/types/affiliateProgramExchanges';

export interface IAffiliateExchangesStartAction {
    type: constants.AFFILIATE_PROGRAM_EXCHANGES_START;
}

export interface IGetAffiliateExchangesAction {
    type: constants.GET_AFFILIATE_EXCHANGES;
    payload: {
        page: number;
    }
}

export interface IGetAffiliateExchangesSuccessAction {
    type: constants.GET_AFFILIATE_EXCHANGES_SUCCESS;
    payload: {
        exchanges: Array<ItemExchange>;
        pages: number;
    };
}

export interface IGetAffiliateExchangesFailAction {
    type: constants.GET_AFFILIATE_EXCHANGES_FAIL;
    payload: {
        error: string;
    };
}

export function getAffiliateExchanges(page: number): IGetAffiliateExchangesAction {
    return {
        type: constants.GET_AFFILIATE_EXCHANGES,
        payload: {
            page
        }
    };
}


export type AffiliateExchangesAction =
    | IAffiliateExchangesStartAction
    | IGetAffiliateExchangesAction
    | IGetAffiliateExchangesSuccessAction
    | IGetAffiliateExchangesFailAction;

