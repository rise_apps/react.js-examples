import * as constants from '@app/constants';

export interface IGetTransactionsAction {
    type: constants.GET_TRANSACTIONS;
    payload:{
        search?: string,
        page?: number
    }
}

export interface IGetTransactionsStartAction {
    type: constants.GET_TRANSACTIONS_START;
}

export interface IGetTransactionsSuccessAction {
    type: constants.GET_TRANSACTIONS_SUCCESS;
    payload: {
        transactions: [],
        pages: number
    };
}

export interface IGetTransactionsFailAction {
    type: constants.GET_TRANSACTIONS_FAIL;
    payload: {
        error: string;
    };
}

export interface IGetOrdersCSVAction {
    type: constants.GET_ORDERS_CSV;
}

export interface IGetOrdersCSVSuccessAction {
    type: constants.GET_ORDERS_CSV_SUCCESS;
    payload: {
        result: any
    };
}

export interface IGetOrdersCSVFailAction {
    type: constants.GET_ORDERS_CSV_FAIL;
    payload: {
        error: string;
    };
}

export function getTransactions(search?:string, page?: number): IGetTransactionsAction {
    return {
        type: constants.GET_TRANSACTIONS,
        payload:{
            search: search,
            page: page
        }
    };
}

export function getOrdersCSV(): IGetOrdersCSVAction {
    return {
        type: constants.GET_ORDERS_CSV,
    };
}

export type TransactionsAction =
    | IGetTransactionsAction
    | IGetTransactionsStartAction
    | IGetTransactionsSuccessAction
    | IGetTransactionsFailAction
    | IGetOrdersCSVAction
    | IGetOrdersCSVFailAction
    | IGetOrdersCSVSuccessAction;

