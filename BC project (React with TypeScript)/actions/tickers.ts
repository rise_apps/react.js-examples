import * as constants from '@app/constants';

export interface IGetTickersAction {
  type: constants.GET_TICKERS;
}

export interface IGetTickersStartAction {
  type: constants.GET_TICKERS_START;
}

export interface IGetTickersSuccessAction {
  type: constants.GET_TICKERS_SUCCESS;
  payload: {
    tickers: [];
  };
}

export interface IGetTickersFailAction {
  type: constants.GET_TICKERS_FAIL;
  payload: {
    error: string;
  };
}

export function getTickers(): IGetTickersAction {
  return {
    type: constants.GET_TICKERS
  };
}

export type TickersAction =
  | IGetTickersAction
  | IGetTickersStartAction
  | IGetTickersSuccessAction
  | IGetTickersFailAction;
