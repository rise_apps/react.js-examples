import * as constants from '@app/constants';
import { Summary, UpdateResponse } from '@app/types/affiliateProgramSummary';

export interface IAffiliateProgramStartAction {
  type: constants.AFFILIATE_PROGRAMM_START;
}

export interface IGetAffiliateSummaryAction {
  type: constants.GET_AFFILIATE_SUMMARY;
}

export interface IGetAffiliateReferralIdAction {
  type: constants.GET_AFFILIATE_REFERRAL_ID;
}

export interface IGetAffiliateReferralIdSuccessAction {
  type: constants.GET_AFFILIATE_REFERRAL_ID_SUCCESS;
  payload: {
    referral_id: string;
  };
}

export interface IClearAffiliateProgramStatusesAction {
  type: constants.CLEAR_AFFILIATE_PROGRAM_STATUSES;
}

export interface IGetAffiliateSummarySuccessAction {
  type: constants.GET_AFFILIATE_SUMMARY_SUCCESS;
  payload: {
    summary: Summary;
  };
}

export interface IUpdateAffiliateProgramStartAction {
  type: constants.ACTION_AFFILIATE_PROGRAM_START;
}

export interface IGetAffiliateSummaryFailAction {
  type: constants.GET_AFFILIATE_SUMMARY_FAIL;
  payload: {
    error: string;
  };
}

export interface IUpdateAffiliateProgramAction {
  type: constants.UPDATE_AFFILIATE_PROGRAM;
  payload: {
    btc_payout_address: string;
  };
}

export interface IUpdateAffiliateProgramSuccessAction {
  type: constants.UPDATE_AFFILIATE_PROGRAM_SUCCESS;
  payload: UpdateResponse;
}

export interface IUpdateAffiliateProgramFailAction {
  type: constants.UPDATE_AFFILIATE_PROGRAM_FAIL;
  payload: {
    error: string;
  };
}

export interface ISchedulePayoutAffiliateProgramAction {
  type: constants.SCHEDULE_PAYOUT_AFFILIATE_PROGRAM;
}

export interface ISchedulePayoutAffiliateProgramSuccessAction {
  type: constants.SCHEDULE_PAYOUT_AFFILIATE_PROGRAM_SUCCESS;
}

export interface ISchedulePayoutAffiliateProgramFailAction {
  type: constants.SCHEDULE_PAYOUT_AFFILIATE_PROGRAM_FAIL;
  payload: {
    error: string;
  };
}

export interface ISetReceivedReferalId {
  type: constants.SET_RECEIVED_REFERAL_ID;
  payload: {
    referal_id: string;
  };
}

export interface IStoreReceivedReferalId {
  type: constants.STORE_RECEIVED_REFERAL_ID;
  payload: {
    referral_id_received: string;
  };
}

export function getAffiliateSummary(): IGetAffiliateSummaryAction {
  return {
    type: constants.GET_AFFILIATE_SUMMARY,
  };
}

export function getReferralId(): IGetAffiliateReferralIdAction {
  return {
    type: constants.GET_AFFILIATE_REFERRAL_ID,
  };
}

export function clearAffiliateProgramStatuses(): IClearAffiliateProgramStatusesAction {
  return {
    type: constants.CLEAR_AFFILIATE_PROGRAM_STATUSES,
  };
}

export function updateAffiliateProgram(
  btc_payout_address: string,
): IUpdateAffiliateProgramAction {
  return {
    type: constants.UPDATE_AFFILIATE_PROGRAM,
    payload: {
      btc_payout_address,
    },
  };
}

export function schedulePayoutAffiliateProgram(): ISchedulePayoutAffiliateProgramAction {
  return {
    type: constants.SCHEDULE_PAYOUT_AFFILIATE_PROGRAM,
  };
}

export function setReceivedReferalId(
  referal_id: string,
): ISetReceivedReferalId {
  return {
    type: constants.SET_RECEIVED_REFERAL_ID,
    payload: {
      referal_id,
    },
  };
}

export type AffiliateSummaryAction =
  | IGetAffiliateSummaryAction
  | IAffiliateProgramStartAction
  | IGetAffiliateSummarySuccessAction
  | IGetAffiliateSummaryFailAction
  | IGetAffiliateReferralIdAction
  | IGetAffiliateReferralIdSuccessAction
  | IUpdateAffiliateProgramAction
  | IUpdateAffiliateProgramStartAction
  | IUpdateAffiliateProgramSuccessAction
  | IUpdateAffiliateProgramFailAction
  | ISchedulePayoutAffiliateProgramAction
  | ISchedulePayoutAffiliateProgramSuccessAction
  | ISchedulePayoutAffiliateProgramFailAction
  | IClearAffiliateProgramStatusesAction
  | ISetReceivedReferalId
  | IStoreReceivedReferalId;
