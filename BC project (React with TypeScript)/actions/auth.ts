import * as constants from '@app/constants';

export interface IAuthAction {
  type: constants.AUTH_USER;
  payload: {
    email: string;
    password?: string;
    socialType?: string;
    accessToken?: string;
  };
}

export interface IAuthStartAction {
  type: constants.AUTH_START;
}

export interface IAuthSuccessAction {
  type: constants.AUTH_SUCCESS;
  payload: {
    accessToken: string;
    expiresIn: number;
  };
}

export interface IAuthFailAction {
  type: constants.AUTH_FAIL;
  payload: {
    error: string;
  };
}

export interface IAuthLogoutAction {
  type: constants.AUTH_LOGOUT;
}

export function login(
  email: string,
  password: string = null,
  socialType: string = null,
  accessToken: string = null,
): IAuthAction {
  return {
    type: constants.AUTH_USER,
    payload: {
      email,
      password,
      socialType,
      accessToken,
    },
  };
}

export function logout(): IAuthLogoutAction {
  return {
    type: constants.AUTH_LOGOUT,
  };
}

export type AuthAction =
  | IAuthAction
  | IAuthStartAction
  | IAuthSuccessAction
  | IAuthFailAction
  | IAuthLogoutAction;
