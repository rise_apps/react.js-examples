import ITransactions from '@app/types/transactions';
import * as actionTypes from '@app/constants';
import { TransactionsAction } from '@app/actions/transactions';

const initialState: ITransactions = {
    data: [],
    loading: false,
    csv: "",
    error: "",
    pages: 1
};

export default function getTickers(
    state: ITransactions = initialState,
    action: TransactionsAction
): ITransactions {
    switch (action.type) {
        case actionTypes.GET_TRANSACTIONS_START:
            return { ...state, loading: true };
        case actionTypes.GET_TRANSACTIONS_SUCCESS:
            return {
                ...state,
                data: action.payload.transactions,
                pages: action.payload.pages,
                loading: false
            };
        case actionTypes.GET_TRANSACTIONS_FAIL:
            return { ...state, error: action.payload.error, loading: false };
        case actionTypes.GET_ORDERS_CSV_SUCCESS:
            return {
                ...state,
                csv: action.payload.result,
            };
        case actionTypes.GET_ORDERS_CSV_FAIL:
            return { ...state, error: action.payload.error, loading: false };
        default:
            return state;
    }
}
