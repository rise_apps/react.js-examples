import IAuth from '@app/types/auth';
import * as actionTypes from '@app/constants';
import { AuthAction } from '@app/actions/auth';

const initialState: IAuth = {
  accessToken: '',
  expiresIn: 0,
  loading: false,
  error: '',
};

export default function auth(
  state: IAuth = initialState,
  action: AuthAction,
): IAuth {
  switch (action.type) {
    case actionTypes.AUTH_SUCCESS:
      return {
        ...state,
        accessToken: action.payload.accessToken,
        expiresIn: action.payload.expiresIn,
        loading: false,
      };
    case actionTypes.AUTH_START:
      return { ...state, loading: true };
    case actionTypes.AUTH_FAIL:
      return { ...state, error: action.payload.error, loading: false };
    case actionTypes.AUTH_LOGOUT:
      return initialState;
    default:
      return state;
  }
}
