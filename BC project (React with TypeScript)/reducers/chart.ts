import IChart from '@app/types/chart';
import * as actionTypes from '@app/constants';
import { ChartAction } from '@app/actions/chart';

const initialState: IChart = {
    data: [],
    products: [],
    productsLoading: false,
    loading: false,
    error: ""
};

export default function getChart(
    state: IChart = initialState,
    action: ChartAction
): IChart {
    switch (action.type) {
        case actionTypes.GET_CHART_DATA_START:
            return { ...state, loading: true };
        case actionTypes.GET_CHART_DATA_SUCCESS:
            return {
                ...state,
                data: action.payload.data,
                loading: false
            };
        case actionTypes.GET_CHART_DATA_FAIL:
            return { ...state, error: action.payload.error, loading: false, productsLoading:false };
        case actionTypes.GET_LIST_PRODUCTS_START:
            return { ...state, productsLoading: true };
        case actionTypes.GET_LIST_PRODUCTS_SUCCESS:
            return {
                ...state,
                products: action.payload.products,
                productsLoading: false
            };
        default:
            return state;
    }
}
