import ITickers from '@app/types/tickers';
import * as actionTypes from '@app/constants';
import { TickersAction } from '@app/actions/tickers';

const initialState: ITickers = {
  data: [],
  loading: false,
  error: ""
};

export default function getTickers(
  state: ITickers = initialState,
  action: TickersAction
): ITickers {
  switch (action.type) {
    case actionTypes.GET_TICKERS_START:
      return { ...state, loading: true };
    case actionTypes.GET_TICKERS_SUCCESS:
      return {
        ...state,
        data: action.payload.tickers,
        loading: false
      };
    case actionTypes.GET_TICKERS_FAIL:
      return { ...state, error: action.payload.error, loading: false };
    default:
      return state;
  }
}
