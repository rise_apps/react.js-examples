import ICurrencies from '@app/types/currencies';
import * as actionTypes from '@app/constants';
import { CurrenciesAction } from '@app/actions/currencies';

const initialState: ICurrencies = {
  types: {
    all: {
      loading: false,
      error: '',
      list: [],
    },
    depositable: {
      loading: false,
      error: '',
      list: [],
    },
    withdrawable: {
      loading: false,
      error: '',
      list: [],
    },
  },
};

export default function currencies(
  state: ICurrencies = initialState,
  action: CurrenciesAction,
): ICurrencies {
  switch (action.type) {
    case actionTypes.GET_LIST_OF_CURRENCIES:
      return {
        ...state,
        types: {
          ...state.types,
          [action.payload.type]: { loading: true },
        },
      };
    case actionTypes.GET_LIST_OF_CURRENCIES_SUCCESS:
      return {
        ...state,
        types: {
          ...state.types,
          [action.payload.type]: {
            list: action.payload.data,
            loading: false,
            error: '',
          },
        },
      };
    case actionTypes.GET_LIST_OF_CURRENCIES_FAIL:
      return {
        ...state,
        types: {
          ...state.types,
          [action.payload.type]: {
            loading: false,
            error: action.payload.error,
          },
        },
      };
    default:
      return state;
  }
}
