import IAffiliate from '@app/types/affiliate';
import * as actionTypes from '@app/constants';
import { AffiliateSummaryAction} from '@app/actions/affiliateProgram';
import { AffiliateExchangesAction }  from '@app/actions/affiliateProgramExchanges';


const initialState: IAffiliate = {
    data: {
        profit: {
            amount: null,
            currency: '',
        },
        payout: {
            amount: null,
            currency: '',
        },
        total_transactions: null,
        percent_revenue_share: null,
    },
    referral_id: '',
    referral_id_received: '',
    updateResponse:{
        affiliate_program_account_id: '',
        account_id: '',
        btc_payout_address: '',
        referral_id: '',
    },
    exchanges: [],
    pages: 1,
    schedulePayoutSuccess: false,
    actionLoading: false,
    exchangesLoading: false,
    loading: false,
    error: ""
};

export default function getAffiliateProgram(
    state: IAffiliate = initialState,
    action: AffiliateSummaryAction | AffiliateExchangesAction
): IAffiliate {
    switch (action.type) {
        case actionTypes.AFFILIATE_PROGRAMM_START:
            return { ...state, loading: true };
        case actionTypes.GET_AFFILIATE_SUMMARY_SUCCESS:
            return {
                ...state,
                data: action.payload.summary,
                loading: false
            };
        case actionTypes.GET_AFFILIATE_SUMMARY_FAIL:
            return { ...state, error: action.payload.error, loading: false };
        case actionTypes.CLEAR_AFFILIATE_PROGRAM_STATUSES:
            return {
                ...state,
                error: "",
                loading: false,
                actionLoading: false,
                schedulePayoutSuccess:false,
                updateResponse: {
                    affiliate_program_account_id: "",
                    account_id: "",
                    btc_payout_address: "",
                    referral_id: ""
                },
            };
        case actionTypes.GET_AFFILIATE_REFERRAL_ID_SUCCESS:
            return {
                ...state,
                referral_id: action.payload.referral_id,
                loading: false,
            };
        case actionTypes.STORE_RECEIVED_REFERAL_ID:
            return {
                ...state,
                referral_id_received: action.payload.referral_id_received,
            };
        case actionTypes.ACTION_AFFILIATE_PROGRAM_START:
            return { ...state, actionLoading: true };
        case actionTypes.UPDATE_AFFILIATE_PROGRAM_SUCCESS:
            return {
                ...state,
                updateResponse: action.payload,
                referral_id: action.payload.referral_id,
                actionLoading: false,
            };
        case actionTypes.UPDATE_AFFILIATE_PROGRAM_FAIL:
            return { ...state, error: action.payload.error, actionLoading: false, };
        case actionTypes.SCHEDULE_PAYOUT_AFFILIATE_PROGRAM_SUCCESS:
            return {
                ...state,
                schedulePayoutSuccess: true,
                actionLoading: false,
            };
        case actionTypes.SCHEDULE_PAYOUT_AFFILIATE_PROGRAM_FAIL:
            return { ...state, error: action.payload.error, actionLoading: false, };
        case actionTypes.AFFILIATE_PROGRAM_EXCHANGES_START:
            return { ...state, exchangesLoading: true };
        case actionTypes.GET_AFFILIATE_EXCHANGES_SUCCESS:
            return {
                ...state,
                exchanges: action.payload.exchanges,
                pages: action.payload.pages,
                exchangesLoading: false,
            };
        case actionTypes.GET_AFFILIATE_EXCHANGES_FAIL:
            return { ...state, error: action.payload.error, loading: false };
        default:
            return state;
    }
}
