import axios from 'axios';
import { IGetChartDataAction, IGetListProductsAction } from '@app/actions/chart';
import * as actionTypes from '@app/constants';
import { put } from 'redux-saga/effects';

export function* getChartDataSaga(action: IGetChartDataAction): Iterator<any> {
    yield put({ type: actionTypes.GET_CHART_DATA_START });
    const url = `/market/products/${action.payload.productId}/ohlcvs`;

    try {
        const {
            data: { data }
        } = yield axios.get(url, {
            params:{
                period: action.payload.period
            }
        });
        yield put({
            type: actionTypes.GET_CHART_DATA_SUCCESS,
            payload: {
                data
            }
        });
    } catch (error) {
        yield put({
            type: actionTypes.GET_CHART_DATA_FAIL,
            payload: { error }
        });
    }
}

export function* getListProductsSaga(
    action: IGetListProductsAction
): Iterator<any> {
    yield put({ type: actionTypes.GET_LIST_PRODUCTS_START });
    const url = `/market/products`;

    try {
        const {
            data: { data }
        } = yield axios.get(url);

        yield put({
            type: actionTypes.GET_LIST_PRODUCTS_SUCCESS,
            payload: {
                products: data
            }
        });
    } catch (error) {
        yield put({
            type: actionTypes.GET_CHART_DATA_FAIL,
            payload: { error }
        });
    }
}