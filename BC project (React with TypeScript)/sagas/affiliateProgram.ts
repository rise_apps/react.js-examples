import axios from 'axios';
import { put } from 'redux-saga/effects';
import {
  IUpdateAffiliateProgramAction,
  IGetAffiliateSummaryAction,
  IGetAffiliateReferralIdAction,
  ISchedulePayoutAffiliateProgramAction,
  ISetReceivedReferalId,
} from '@app/actions/affiliateProgram';
import * as actionTypes from '@app/constants';
import { IGetAffiliateExchangesAction } from '@app/actions/affiliateProgramExchanges';

export function* getAffiliateSummarySaga(
  action: IGetAffiliateSummaryAction,
): Iterator<any> {
  yield put({ type: actionTypes.AFFILIATE_PROGRAMM_START });
  const url = '/affiliate-program/summary';

  try {
    const { data: { data } } = yield axios.get(url);
    yield put({
      type: actionTypes.GET_AFFILIATE_SUMMARY_SUCCESS,
      payload: {
        summary: data,
      },
    });
  } catch (error) {
    yield put({
      type: actionTypes.GET_AFFILIATE_SUMMARY_FAIL,
      payload: { error },
    });
  }
}

export function* getReferralIdSaga(
  action: IGetAffiliateReferralIdAction,
): Iterator<any> {
  yield put({ type: actionTypes.AFFILIATE_PROGRAMM_START });
  const url = '/affiliate-program/accounts/me';

  try {
    const { data } = yield axios.get(url);
    yield put({
      type: actionTypes.GET_AFFILIATE_REFERRAL_ID_SUCCESS,
      payload: {
        referral_id: data.referral_id,
      },
    });
  } catch (error) {
    yield put({
      type: actionTypes.GET_AFFILIATE_SUMMARY_FAIL,
      payload: { error },
    });
  }
}

export function* updateAffiliateProgramSaga(
  action: IUpdateAffiliateProgramAction,
): Iterator<any> {
  yield put({ type: actionTypes.ACTION_AFFILIATE_PROGRAM_START });
  const url = '/affiliate-program/accounts/me';

  try {
    const { data } = yield axios.patch(url, action.payload);

    yield put({
      type: actionTypes.UPDATE_AFFILIATE_PROGRAM_SUCCESS,
      payload: data,
    });
  } catch (error) {
    yield put({
      type: actionTypes.UPDATE_AFFILIATE_PROGRAM_FAIL,
      payload: { error },
    });
  }
}

export function* schedulePayoutAffiliateProgramSaga(
  action: ISchedulePayoutAffiliateProgramAction,
): Iterator<any> {
  yield put({ type: actionTypes.ACTION_AFFILIATE_PROGRAM_START });
  const url = '/affiliate-program/payouts';

  try {
    yield axios.post(url);
    yield put({
      type: actionTypes.SCHEDULE_PAYOUT_AFFILIATE_PROGRAM_SUCCESS,
    });
  } catch (error) {
    yield put({
      type: actionTypes.SCHEDULE_PAYOUT_AFFILIATE_PROGRAM_FAIL,
      payload: { error },
    });
  }
}

export function* getAffiliateExchangesSaga(
  action: IGetAffiliateExchangesAction,
): Iterator<any> {
  yield put({ type: actionTypes.AFFILIATE_PROGRAM_EXCHANGES_START });
  const url = '/affiliate-program/orders';

  try {
    const { data } = yield axios.get(url, {
      params: {
        page: action.payload.page,
      },
    });
    yield put({
      type: actionTypes.GET_AFFILIATE_EXCHANGES_SUCCESS,
      payload: {
        exchanges: data.data,
        pages: data.meta.pagination.total_pages,
      },
    });
  } catch (error) {
    yield put({
      type: actionTypes.GET_AFFILIATE_EXCHANGES_FAIL,
      payload: { error },
    });
  }
}

export function* setReferalIdToSession(
  action: ISetReceivedReferalId,
): Iterator<any> {
  yield sessionStorage.setItem('ref_id', action.payload.referal_id);
  yield put({
    type: actionTypes.STORE_RECEIVED_REFERAL_ID,
    payload: { referral_id_received: action.payload.referal_id },
  });
}
