import axios from 'axios';
import { IGetTransactionsAction, IGetOrdersCSVAction } from '@app/actions/transactions';
import * as actionTypes from '@app/constants';
import { put } from 'redux-saga/effects';

export function* getTransactionsSaga(action: IGetTransactionsAction): Iterator<any> {
    yield put({ type: actionTypes.GET_TRANSACTIONS_START });
    const url = "/exchange/orders";

    try {
        const { data } = yield axios.get(url, {
            params: {
                q: action.payload.search,
                page: action.payload.page
            }
        });
        yield put({
            type: actionTypes.GET_TRANSACTIONS_SUCCESS,
            payload: {
                transactions: data.data,
                pages: data.meta.pagination.total_pages
            }
        });
    } catch (error) {
        yield put({
            type: actionTypes.GET_TRANSACTIONS_FAIL,
            payload: { error }
        });
    }
}

export function* getOrdersCSVSaga(action: IGetOrdersCSVAction): Iterator<any> {
    const url = "/exchange/orders/csv";
    try {
        const { data } = yield axios.get(url);
        yield put({
            type: actionTypes.GET_ORDERS_CSV_SUCCESS,
            payload: {
                result: data
            }
        });
    } catch (error) {
        yield put({
            type: actionTypes.GET_ORDERS_CSV_FAIL,
            payload: { error }
        });
    }
}
