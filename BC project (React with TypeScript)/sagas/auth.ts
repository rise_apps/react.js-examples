import axios from 'axios';
import { IAuthAction } from '@app/actions/auth';
import * as actionTypes from '@app/constants';
import { put, call } from 'redux-saga/effects';

export function* authSaga(action: IAuthAction): Iterator<any> {
  // TODO Delete 'if' when social auth will be added
  if (!action.payload.socialType) {
    yield put({ type: actionTypes.AUTH_START });
    const url = '/iam/tokens';
    const body = {
      email: action.payload.email,
      password: action.payload.password,
    };

    try {
      const { data: { data } } = yield axios.post(url, body);

      yield call(setSessionSaga, data.access_token, data.expires_in);
      yield put({
        type: actionTypes.AUTH_SUCCESS,
        payload: {
          accessToken: data.access_token,
          expiresIn: data.expires_in,
        },
      });
    } catch (error) {
      yield put({
        type: actionTypes.AUTH_FAIL,
        payload: { error },
      });
    }
  } else {
    yield call(setSessionSaga, action.payload.accessToken, '86400');
    yield put({
      type: actionTypes.AUTH_SUCCESS,
      payload: {
        accessToken: action.payload.accessToken,
        expiresIn: '86400',
      },
    });
  }
}

function* setSessionSaga(
  accessToken: string,
  expiresIn: string,
): Iterator<any> {
  yield sessionStorage.setItem('accessToken', accessToken);
  yield sessionStorage.setItem('expiresIn', expiresIn);
}

export function* authCheckSessionStorageSaga(): Iterator<any> {
  const accessToken = yield sessionStorage.getItem('accessToken');
  const ref_id = yield sessionStorage.getItem('ref_id');

  if (ref_id) {
    yield put({
      type: actionTypes.STORE_RECEIVED_REFERAL_ID,
      payload: { referral_id_received: ref_id },
    });
  }

  if (!accessToken) {
    yield put({ type: actionTypes.AUTH_LOGOUT });
  } else {
    const expiresIn = yield new Date(sessionStorage.getItem('expiresIn'));

    if (expiresIn <= new Date()) {
      yield put({ type: actionTypes.AUTH_LOGOUT });
    } else {
      // Success login from session
      yield put({
        type: actionTypes.AUTH_SUCCESS,
        payload: {
          accessToken,
          expiresIn,
        },
      });
    }
  }
}

export function* clearSessionStorageSaga(): Iterator<any> {
  yield sessionStorage.removeItem('accessToken');
  yield sessionStorage.removeItem('expiresIn');
}
