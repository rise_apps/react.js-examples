import axios from 'axios';
import { IGetListCurrenciesAction } from '@app/actions/currencies';
import { put, call } from 'redux-saga/effects';
import * as actionTypes from '@app/constants';

export function* getListCurrenciesSaga(
  action: IGetListCurrenciesAction,
): Iterator<any> {
  const url = '/exchange/currencies';
  const { type } = action.payload;
  const query = yield call(getQueryOfCurrenciesSaga, type);

  try {
    const { data: { data } } = yield axios.get(url, query);
    yield put({
      type: actionTypes.GET_LIST_OF_CURRENCIES_SUCCESS,
      payload: {
        type,
        data,
      },
    });
  } catch (error) {
    yield put({
      type: actionTypes.GET_LIST_OF_CURRENCIES_FAIL,
      payload: { error, type },
    });
  }
}

export function* getQueryOfCurrenciesSaga(type: string): Iterator<any> {
  const query = {
    params: {
      depositable: true,
      withdrawable: true,
    },
  };

  if (type === 'depositable') {
    query.params.withdrawable = false;
  } else if (type === 'withdrawable') {
    query.params.depositable = false;
  }

  return query;
}
