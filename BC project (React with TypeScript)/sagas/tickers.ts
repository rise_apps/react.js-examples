import axios from 'axios';
import { IGetTickersAction } from '@app/actions/tickers';
import * as actionTypes from '@app/constants';
import { put } from 'redux-saga/effects';

export function* getTickersSaga(action: IGetTickersAction): Iterator<any> {
  yield put({ type: actionTypes.GET_TICKERS_START });
  const url = "/market/tickers";

  try {
    const {
      data: { data }
    } = yield axios.get(url);
    yield put({
      type: actionTypes.GET_TICKERS_SUCCESS,
      payload: {
        tickers: data
      }
    });
  } catch (error) {
    yield put({
      type: actionTypes.GET_TICKERS_FAIL,
      payload: { error }
    });
  }
}
