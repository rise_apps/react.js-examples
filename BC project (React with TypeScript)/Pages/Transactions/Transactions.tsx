import React from "react";
import styled, { css } from "react-emotion";
import BackDrop from "@app/components/BackDrop";
import { WidgetMin } from "@app/components/WidgetExchange";
import Svg from "@app/assets/images/preloadPayment.svg";
import Table from "@app/components/Table";
import { ColumnProps } from "antd/lib/table";
import Icon from "@app/components/Icon";
import Button from "@app/components/Button";
import { Input } from "antd";
import IStoreState from "@app/types";
import { Dispatch } from "redux";
import * as actions from "@app/actions/transactions";
import { connect } from "react-redux";
import { RouteComponentProps } from "react-router-dom";
import { Orders } from "@app/types/transactions";
import { addSeconds, format } from "date-fns";

import { translate } from 'react-i18next';
import { TranslationFunction } from 'i18next';
import Preloader from '@app/components/Preloader';
import { debounce } from 'ts-debounce';

interface ITransactionsProps {
  transactions: Array<Orders>;
  testData?: any;
  getTransactions: (search: string, page: number) => void;
  loading: boolean;
  t: TranslationFunction;
  pages: number;
  csv: string;
  getCSV: () => void;
}

class Transactions extends React.Component<
  ITransactionsProps & RouteComponentProps<{}>
> {
  state = {
    searchText: "",
    pagination: {
      total: 7,
      pageSize: 7,
      current: 1,
    },
  };

  private searchTransaction: any = debounce(() => {
      const { searchText, pagination: { current } } = this.state;
      this.props.getTransactions(searchText, current ? current : 1);
  }, 500);

  componentDidMount() {
    const {
      searchText,
      pagination: { current }
    } = this.state;
    this.props.getTransactions(searchText, current);
    if (!this.props.csv){
        this.props.getCSV();
    }
  }

  componentDidUpdate(prevProps: ITransactionsProps) {
    if (prevProps.pages !== this.props.pages) {
      const { pagination } = this.state;
      pagination.total = this.props.pages * pagination.pageSize;
      this.setState({ pagination });
    }
  }

  getDateFormatString = (seconds: number, formatTo = "dd/MM/YYYY, hh:mm:ss") => {
    const date = addSeconds(new Date(0), seconds);
    return format(date, formatTo);
  };

  getTableItems = (data: Orders[]) => {
    const { searchText } = this.state;
    const formattedItems = data.map((el, i) => {
      const { transactions } = el;
      const isMulti = transactions.length > 1;
      const firstTrans = transactions[0];
      const {final_exchange_rate, seen_exchange_rate, from ,to, fee} = firstTrans;
      const SumOfMultiEAmount = transactions.reduce((c,a)=>{
          return a.from.amount + c
      }, 0);
      const SumOfMultiFee = transactions.reduce((c,a)=>{
          return a.fee ? a.fee.amount + c : c;
      }, 0);
      const NullStub = (<span>&minus;</span>);
      const arrChildren = transactions.map((item, ind)=> {
        return {
            ...item,
            status: item.status,
            date: this.getDateFormatString(item.updated_at),
            exchangeAmount: `${item.from.amount} ${item.from.currency}`,
            fee: `${item.fee.amount} ${item.fee.currency}`,
            rate: `1 ${item.from.currency} = ${item.final_exchange_rate ? item.final_exchange_rate : item.seen_exchange_rate} ${item.to.currency}`,
            receiver: item.to && `${item.to.address.substring(
                0,
                3
            )}...${item.to.address.substr(item.to.address.length - 3)}`,
            amountReceived: item.to ? `${item.to.amount} ${item.to.currency}`: NullStub,
            key: `${i}${ind}`,
        }
      });
      return {
          ...el,
          status: isMulti? 'multi_exchange': el.state,
          date: isMulti? this.getDateFormatString(el.updated_at, 'dd/MM/YYYY') : this.getDateFormatString(el.updated_at),
          exchangeAmount: isMulti? `${SumOfMultiEAmount} ${from.currency}` :`${from.amount} ${from.currency}`,
          fee: isMulti? `${SumOfMultiFee} ${fee.currency}` : `${fee.amount} ${fee.currency}`,
          rate: isMulti? '' : `1 ${from.currency} = ${final_exchange_rate ? final_exchange_rate : seen_exchange_rate} ${to.currency}`,
          receiver: to && isMulti ? '' : `${to.address.substring(
              0,
              3
          )}...${to.address.substr(to.address.length - 3)}`,
          amountReceived: isMulti? '' : to ? `${to.amount} ${to.currency}` : NullStub,
          children: isMulti ? arrChildren :
              null,
          key: i
      };
    });
    const filtered = formattedItems.filter(
      el =>
        Object.values(el)
          .join()
          .toLowerCase()
          .indexOf(searchText.toLowerCase()) >= 0
    );
    if (searchText !== "") {
      return filtered;
    }
    return formattedItems;
  };

  renderStatus = (text: string, record: Orders, index: number) => {
    let StatusIcon = null;
    let color = null;
    switch (text) {
      case "new":
        StatusIcon = null;
        color = "#0054d7";
        break;
      case "multi_exchange":
        StatusIcon = null;
        color = "#8b99b1";
        break;
      case "complete":
        StatusIcon = <Icon name="check" />;
        color = "#00a046";
        break;
      case "exchanging":
        StatusIcon = <Icon name="time" />;
        color = "#5fb1b1";
        break;
      case "withdrawing":
        StatusIcon = <Icon name="time" />;
        color = "#5fb1ff";
        break;
      case "awaiting_deposit":
        StatusIcon = <Icon name="time" />;
        color = "#8b99b1";
        break;
      case "processing":
        StatusIcon = <Icon name="time" />;
        color = "#f1b349";
        break;
      case "expired":
        StatusIcon = <Icon name="time" />;
        color = "#ff6464";
        break;
    }
    return (
      <StatusCol style={{ color }}>
        {StatusIcon}
        {` `}
        {this.props.t(`Statuses=>${text.split("_").join(" ")}`)}
      </StatusCol>
    );
  };

  onSearch = (e: any): void => {
    this.setState({ [e.target.name]: e.target.value });
    this.searchTransaction();
  };

  handleTableChange = (pagination: any, filters: any, sorter: any) => {
    const {
      searchText,
      pagination: { current }
    } = this.state;
    if (pagination.current !== current) {
      this.props.getTransactions(searchText, pagination.current);
      this.setState({ pagination });
    }
  };

  public render() {
    const { transactions, loading, t, testData, csv } = this.props;
    const { searchText, pagination } = this.state;
    const columns: ColumnProps<Orders>[] = [
      {
        title: t("Transactions=>Status"),
        dataIndex: "status",
        key: "status",
        sorter: (a: any, b: any) => a.status.length - b.status.length,
        render: (text, record, index) => this.renderStatus(text, record, index)
        // render: (text, record, index) => record.state
      },
      {
        title: t("Transactions=>Date"),
        dataIndex: "date",
        key: "date"
      },
      {
        title: t("Transactions=>Exchange amount"),
        dataIndex: "exchangeAmount",
        key: "exchangeAmount",
        render: text => <BoldCol>{text}</BoldCol>
      },
      {
        title: t("Transactions=>Total fee"),
        dataIndex: "fee",
        key: "fee"
      },
      {
        title: t("Transactions=>Exchange rate"),
        dataIndex: "rate",
        key: "rate"
      },
      {
        title: t("Transactions=>Receiver"),
        dataIndex: "receiver",
        key: "receiver",
        render: text => <a href="javascript:">{text}</a>
      },
      {
        title: t("Transactions=>Amount received"),
        dataIndex: "amountReceived",
        key: "amountReceived",
        render: text => <BoldCol>{text}</BoldCol>
      }
    ];
    const dataSource: any = testData ? this.getTableItems(testData) : this.getTableItems(transactions);
    return (
      <Wrapper>
        <BackDrop variant="minimal" />
        <Container>
          <WidgetMin />
          <TransactionHistoryWrap>
            <TableWrap>
              {!loading && !transactions.length && !testData ? (
                <Stub>
                  <WrapIcon>
                    <ImgSvg src={Svg} />
                    <InnerIcon />
                  </WrapIcon>
                  <StubLabel>
                    {t(
                      "Transactions=>Your transaction history will be displayed here"
                    )}
                  </StubLabel>
                </Stub>
              ) : (
                <React.Fragment>
                  <SearchWrap>
                    <Input
                      className={searchStyle}
                      name="searchText"
                      value={searchText}
                      placeholder={t("Transactions=>Search")}
                      onChange={this.onSearch}
                    />
                      {csv && (
                          <Button ghost type="primary" htmlType="submit">
                              <a
                                  href={csv}
                                  download={`csv-orders-${format(
                                      new Date(),
                                      "dd-MM-YYYY"
                                  )}.csv`}
                              >
                                  <Icon name="excl" /> {t("Transactions=>Export to")}{" "}
                                  .CSV
                              </a>
                          </Button>
                      )}
                  </SearchWrap>
                  <Table
                    loading={{
                      spinning: loading,
                      indicator: <Preloader />
                    }}
                    dataSource={dataSource}
                    columns={columns}
                    pagination={pagination}
                    scroll={{ x: 896 }}
                    onChange={this.handleTableChange}
                  />
                </React.Fragment>
              )}
            </TableWrap>
          </TransactionHistoryWrap>
        </Container>
      </Wrapper>
    );
  }
}

function mapStateToProps({ transactions }: IStoreState) {
  return {
    loading: transactions.loading,
    error: transactions.error,
    transactions: transactions.data,
    pages: transactions.pages,
    csv: transactions.csv,
  };
}

function mapDispatchToProps(dispatch: Dispatch<actions.TransactionsAction>) {
  return {
    getTransactions: (search?: string, page?: number) =>
      dispatch(actions.getTransactions(search, page)),
    getCSV: () => dispatch(actions.getOrdersCSV())
  };
}

export default translate()(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(Transactions)
);

const Wrapper = styled("div")``;

const SearchWrap = styled("div")`
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding: 16px;
`;
const searchStyle = css`
  max-width: 224px;
  flex: 1 auto;
  &.ant-input {
    margin-right: 8px;
  }
`;

const TableWrap = styled('div')`
  position: relative;
  & .ant-table-placeholder {
    position: absolute;
    width: 100%;
    top: 50%;
    left: 0;
    border: none;
    transform: translateY(-50%);
  }
`;

const StatusCol = styled("div")`
  font-size: 12px;
  font-weight: bold;
  line-height: 20px;
  text-transform: capitalize;
  display: inline-block;
`;

const BoldCol = styled("div")`
  font-weight: bold;
`;

const Container = styled("div")`
  width: 100%;
  max-width: 896px;
  margin: 0 auto;
`;
const TransactionHistoryWrap = styled("div")`
  margin-bottom: 12px;
  background: #fff;
  border-radius: 4px;
  @media (max-width: 896px) {
    margin: 0 -12px 12px;
  }
`;

const Stub = styled("div")`
  padding: 100px 0;
  max-width: 274px;
  margin: 0 auto;
`;

const StubLabel = styled("div")`
  font-family: Roboto Mono;
  font-size: 20px;
  font-weight: bold;
  line-height: 32px;
  letter-spacing: -0.6px;
  text-align: center;
  color: #0f2b58;
`;

const WrapIcon = styled("div")`
  position: relative;
  text-align: center;
  margin-bottom: 40px;
`;

const ImgSvg = styled("img")`
  display: inline-block;
  width: 88px;
  height: 88px;
`;

const InnerIcon = styled("div")`
  width: 44px;
  height: 44px;
  position: absolute;
  left: calc(50% - 22px);
  top: calc(50% - 22px);
  border-radius: 50%;
  background: #cdd3de;
  color: #fff;
  font-size: 24px;
  display: flex;
  align-items: center;
  justify-content: center;
`;
