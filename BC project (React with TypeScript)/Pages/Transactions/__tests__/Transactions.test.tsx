import * as React from "react";
import { mount } from "enzyme";
import i18n from "@app/i18n";
import { I18nextProvider } from "react-i18next";
import { Provider } from "react-redux";
import { BrowserRouter as Router } from "react-router-dom";
import createSagaMiddleware from "redux-saga";
import { applyMiddleware, createStore, Store } from "redux";
import rootReducer from "@app/reducers";
import { setDefaultAxios } from "@app/helpers/axios";
import { mountToJson } from "enzyme-to-json";

// Components
import Transactions from "@app/pages/Transactions";
import { WidgetMin } from "@app/components/WidgetExchange";
import Table from "@app/components/Table";
import { Input } from "antd";

const sagaMiddleware = createSagaMiddleware();
const store: Store<any> = createStore(
  rootReducer,
  applyMiddleware(sagaMiddleware)
);
setDefaultAxios();

store.dispatch = jest.fn();

const data = {
  created_at: 1534448643,
  email: "jsmith@example.com",
  order_id: "78bd0c85-06e7-4e01-aca3-e02eb110bc8d",
  source: "website",
  state: "new",
  transactions: [
    {
      created_at: 1534448643,
      fee: { amount: 0.001, currency: "ETH" },
      final_exchange_rate: "",
      from: { address: "", amount: 2.1, currency: "ETH" },
      profit: { amount: 0.0623, currency: "ETH" },
      seen_exchange_rate: 0.0352,
      status: "new",
      to: {
        address: "n4VQ5YdHf7hLQ2gWQYYrcxoE5B7nWuDFNF",
        amount: 0.074,
        currency: "BTC"
      },
      transaction_id: "b4f7c5e1-c5c8-4a25-a293-f1d72ed22937",
      updated_at: 1534448643
    }
  ],
  updated_at: 1537798137,
  status: "new",
  profit: "0.0623 ETH",
  exchangeAmount: "2.1 ETH",
  amountReceived: "0.074 BTC",
  children: "",
  key: 0
};

const arrayData = [data, data, { ...data, state: "complete" }];

{
  /*TODO: resolve error*/
}
//@ts-ignore
const Trans = <Transactions />;
//@ts-ignore
const TransWithData = <Transactions testData={arrayData} />;

const getComponent = (transactions: any) =>
  mount(
    <Provider store={store}>
      <Router>
        <I18nextProvider i18n={i18n}>{transactions}</I18nextProvider>
      </Router>
    </Provider>
  )
    .find(Transactions)
    .childAt(0)
    .childAt(0)
    .childAt(0);

const component = getComponent(Trans),
  componentWithData = getComponent(TransWithData);

describe("Transactions", () => {
  it("should have state", () => {
    expect(component.instance().state).toEqual({
      searchText: "",
      pagination: {
        total: 7,
        pageSize: 7,
        current: 1
      }
    });
  });

  it("should been preload image", () => {
    expect(component.find("img").prop("src")).toEqual("preloadPayment.svg");
    expect(component.find(WidgetMin).exists()).toBeTruthy();
  });

  it("should have error equal false", () => {
    expect(component.prop("error")).toBeFalsy();
  });

  it("render correctly data", () => {
    expect(mountToJson(component)).toMatchSnapshot();
  });

  it("should call componentDidMount", async () => {
    const spy = jest.spyOn(component.instance(), "componentDidMount");
    await component.instance().componentDidMount();
    await expect(spy).toHaveBeenCalled();
  });

  it("should call getTableItems", () => {
    //@ts-ignore
    const spy = jest.spyOn(component.instance(), "getTableItems");
    component.instance().render();
    expect(spy).toHaveBeenCalled();
  });

  it("should render table", async () => {
    expect(componentWithData.find(Table).exists()).toBeTruthy();
    expect(mountToJson(component)).toMatchSnapshot();
  });

  it("should work search", () => {
    //@ts-ignore
    expect(componentWithData.instance().state.searchText).toBeFalsy();
    expect(componentWithData.find(Input).props().value).toBe("");
    //@ts-ignore
    componentWithData.instance().onSearch({
      target: {
        name: 'searchText',
        value: 'compl'
      }
    });
    componentWithData.instance().forceUpdate();
    //@ts-ignore
    expect(componentWithData.find(Input).instance().props.value).toBe('compl');
    //@ts-ignore
    expect(componentWithData.instance().state.searchText).toBe('compl');
  });

  it("should have correct dispatch", function() {
    //@ts-ignore
    expect(store.dispatch.mock.calls).toMatchSnapshot();
  });
});
