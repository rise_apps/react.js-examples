import AsteriaAuthenticatedClient from '../../clients/AsteriaAuthenticatedClient';

export const fetchCompanyGraph = () => dispatch => {
	dispatch({ type: 'COMPANY_GRAPH_FETCH' });
	return AsteriaAuthenticatedClient.get('/company/graph')
		.then(({ data }) => data)
		.then(({ payload }) => {
			dispatch({ type: 'COMPANY_GRAPH_LOADED', payload });
		})
		.catch(payload => {
			dispatch({ type: 'COMPANY_GRAPH_FAILED', payload });
		});
};

const shouldFetchCompanyGraph = state => {
	if (state.companyGraph.isFetching) {
		return false;
	} else if (
		state.companyGraph.companyGraph &&
		state.companyGraph.companyGraph.view
	) {
		return false;
	}

	return true;
};
