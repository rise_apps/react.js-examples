import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { Button } from '../../../components/shared/buttons/buttons';

class CustomerInvoicesRangeSlider extends Component {
  state = {
    isDraging: false,
    activeItems: [],
    oldX: 0,
    firstClick: true,
  };

  /**
   * On mouse move set active items from right or left side
   * and set current mouse X position
   * @param {Event} e
   * @param {Number} index
   */
  onMouseMove = (e, index) => {
    const { isDraging, oldX, activeItems } = this.state;
    const { dataItems } = this.props;

    if (isDraging) {
      if (e.pageX < oldX) {
        if (
          index + 1 < dataItems.length &&
          this.isItemActive(dataItems[index + 1])
        ) {
          // Left side activate
          if (!this.isItemActive(dataItems[index])) {
            activeItems.unshift(dataItems[index]);
          }
        } else if (index - 1 > 0 && this.isItemActive(dataItems[index - 1])) {
          // Left side deactivate
          if (this.isItemActive(dataItems[index])) {
            activeItems.pop();
          }
        }
      } else if (e.pageX > oldX) {
        if (index - 1 > 0 && this.isItemActive(dataItems[index - 1])) {
          // Right side activate
          if (!this.isItemActive(dataItems[index])) {
            activeItems.push(dataItems[index]);
          }
        } else if (
          index + 1 < dataItems.length &&
          this.isItemActive(dataItems[index + 1])
        ) {
          // Right side deactivate
          if (this.isItemActive(dataItems[index])) {
            activeItems.shift();
          }
        }
      }

      this.setState({
        activeItems,
        oldX: e.pageX,
      });
    }
  };

  /**
   * Double click event for select or unselect range points
   * @param {Event} e
   * @param {Number} index
   */
  onDoubleClick = (e, index) => {
    this.setState({ firstClick: false });
    const activeItems = [];
    const { dataItems } = this.props;

    for (let i = 0; i < dataItems.length; i += 1) {
      if (i === index) {
        activeItems.push(dataItems[i]);
      }
    }

    this.props.dataLoad({
      type: activeItems[0].timelineType,
      startDate: activeItems[0].date,
      endDate: activeItems[0].date,
      accountBalanceStart: activeItems[0].accountData,
      accountBalanceFinish: 0,
    });

    this.setState({
      activeItems,
      oldX: e.pageX,
    });
  };

  onClick = (e, index) => {
    const { activeItems } = this.state;
    const { dataItems } = this.props;

    if (!activeItems.length) {
      return;
    }

    const middleCount = this.countMiddleIndex(activeItems);

    if (middleCount > index) {
      // Left side
      if (this.isItemActive(dataItems[index])) {
        for (let i = 0; i < index; i += 1) {
          if (this.isItemActive(dataItems[i])) {
            activeItems.shift();
          }
        }
      } else {
        for (let i = Math.round(middleCount); i >= index; i -= 1) {
          if (!this.isItemActive(dataItems[i])) {
            activeItems.unshift(dataItems[i]);
          }
        }
      }
    } else if (middleCount < index) {
      // Right side
      if (this.isItemActive(dataItems[index])) {
        for (let i = dataItems.length - 1; i > index; i -= 1) {
          if (this.isItemActive(dataItems[i])) {
            activeItems.pop();
          }
        }
      } else {
        for (let i = Math.round(middleCount); i <= index; i += 1) {
          if (!this.isItemActive(dataItems[i])) {
            activeItems.push(dataItems[i]);
          }
        }
      }
    }

    this.setState({
      activeItems,
      oldX: e.pageX,
    });
  };

  handleStart = e => {
    document.addEventListener('mouseup', this.handleEnd);
    this.setState({
      isDraging: true,
      oldX: e.pageX,
    });
  };

  handleEnd = () => {
    const { activeItems } = this.state;
    const firstActive = activeItems[0];
    const lastActive = activeItems[activeItems.length - 1];

    if (firstActive !== undefined) {
      this.props.dataLoad({
        type: firstActive.timelineType,
        startDate: firstActive.date,
        endDate: lastActive.date,
        accountBalanceStart: firstActive.accountData,
        accountBalanceFinish: lastActive.accountData,
      });
    }

    this.setState({ isDraging: false });
    document.removeEventListener('mouseup', this.handleEnd);
  };

  countMiddleIndex = activeItems => {
    const firstActive = this.findIndex(activeItems[0]);
    const lastActive = this.findIndex(activeItems[activeItems.length - 1]);

    return (firstActive + lastActive) / 2;
  };

  findIndex = item => {
    const { dataItems } = this.props;
    for (const i in dataItems) {
      if (item.label === dataItems[i].label) {
        return +i;
      }
    }
    return NaN;
  };

  /**
   * Output items for range
   */
  mapRangeItems = () => {
    const { dataItems } = this.props;
    if (dataItems) {
      return dataItems.map((item, index) => (
        <Button
          active={this.isItemActive(item)}
          size="small"
          label={item.label}
          key={item.date}
          onMouseDown={e => this.handleStart(e)}
          onMouseMove={e => this.onMouseMove(e, index)}
          onDoubleClick={e => this.onDoubleClick(e, index)}
          onClick={
            this.state.firstClick
              ? e => this.onDoubleClick(e, index)
              : e => this.onClick(e, index)
          }
        />
      ));
    }
    return null;
  };

  isItemActive = item => {
    const { activeItems } = this.state;
    for (const i in activeItems) {
      if (activeItems[i].label === item.label) {
        return true;
      }
    }
    return false;
  };

  render() {
    return (
      <div className="range">
        <Button
          onClick={() => this.props.prevBtn()}
          icon="icomoon-rounded-triangle--filled"
          styleType="default mirror-icon no-label"
          size="small"
        />
        <ul className="btn_group">{this.mapRangeItems()}</ul>
        <Button
          onClick={() => this.props.nextBtn()}
          icon="icomoon-rounded-triangle--filled"
          styleType="default no-label"
          size="small"
        />
      </div>
    );
  }
}

CustomerInvoicesRangeSlider.propTypes = {
  nextBtn: PropTypes.func,
  prevBtn: PropTypes.func,
};

CustomerInvoicesRangeSlider.defaultProps = {
  nextBtn: () => {},
  prevBtn: () => {},
};

export default CustomerInvoicesRangeSlider;
