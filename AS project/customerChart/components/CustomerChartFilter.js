import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { connect } from 'react-redux';
import { setTimeline } from '@/customerInvoices/actions';
import { getTimeline } from '@/customerInvoices/selectors';
import { Select } from '@/components/shared/inputs/selects/select';
import I18n from '@/utils/I18n';

import env from '@/env';

class CustomerInvoicesChartFilter extends Component {
  state = {
    activeFilter: 1,
  };

  renderFilterButtons = HandleSelect =>
    filterArray.map((item, i) => {
      const activeClass =
        this.state.activeFilter === i ? 'chart-filter__btn--active' : '';

      return (
        <button
          className={`chart-filter__btn ${activeClass}`}
          onClick={e => {
            this.setFilter(i);
          }}
          key={item.value}
        >
          {I18n.get(`cashflow.graph.timeslices.${item.value}`)}
        </button>
      );
    });

  render() {
    return (
      <div className="chart-filter">
        <i className="icomoon-calendar" />
        <ul className="legends">
          <li
            onClick={() => this.props.toggleLegend('incoming')}
            className={`item green ${
              this.props.legends.incoming ? 'active' : ''
            }`}
          >
            {I18n.get('cashflow.graph.legends.incoming')}
          </li>
          <li
            onClick={() => this.props.toggleLegend('outgoing')}
            className={`item grey ${
              this.props.legends.outgoing ? 'active' : ''
            }`}
          >
            {I18n.get('cashflow.graph.legends.outgoing')}
          </li>
          <li
            onClick={() => this.props.toggleLegend('account')}
            className={`item blue ${
              this.props.legends.account ? 'active' : ''
            }`}
          >
            {I18n.get('cashflow.graph.legends.account')}
          </li>
        </ul>
        <Select
          size="small"
          type="default"
          selected={I18n.get('cashflow.graph.timeslices.week')}
        >
          {this.renderFilterButtons()}
        </Select>
      </div>
    );
  }
}

CustomerInvoicesChartFilter.propTypes = {
  dispatch: PropTypes.func.isRequired,
};

function mapStateToProps(state) {
  return {
    timeline: getTimeline(state),
    languageVariables: state.languageDropdown.variables,
  };
}

export default connect(mapStateToProps)(CustomerInvoicesChartFilter);
