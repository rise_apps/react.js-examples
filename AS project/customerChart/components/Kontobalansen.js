import React, { Component } from 'react';
import { connect } from 'react-redux';
import '@/styles/kontobalansen.scss';

class Kontobalansen extends Component {
	render() {
		const { accountBalanceStart, accountBalanceFinish } = this.props;
		return (
			<div className="kontoB_wrap">
				<h4 className="title">{I18n.get(`boxes.account.title`)}</h4>
				<div className="kontoB_content">
					<span className="balance intakter">
						{formatNumber(accountBalanceStart)}
					</span>
					{accountBalanceFinish
						? [
								<span
									key="rounded-triangle"
									className="arrow icomoon-rounded-triangle--filled"
								/>,
								<span key="balance" className="balance utgifter">
									{formatNumber(accountBalanceFinish)}
								</span>,
								<div
									key="diff_indicator"
									className="diff_indicator probability-indicator probability-indicator--yellow"
								>
									<div className="probability-indicator__point" />
									<div className="probability-indicator__point" />
									<div className="probability-indicator__point" />
									<div className="probability-indicator__point" />
								</div>,
								<span
									key="rounded-triangle__filled"
									className={`arrow icomoon-rounded-triangle--filled ${accountBalanceStart >
									accountBalanceFinish
										? 'down'
										: 'up'}`}
								/>,
							]
						: null}
				</div>
			</div>
		);
	}
}

function mapStateToProps(state) {
	return {
		accountBalanceStart: state.customerInvoices.accountBalanceStart,
		accountBalanceFinish: state.customerInvoices.accountBalanceFinish,
		languageVariables: state.languageDropdown.variables,
	};
}

Kontobalansen.propTypes = {};
export default connect(mapStateToProps)(Kontobalansen);
