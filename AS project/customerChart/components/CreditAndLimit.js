import React, { Component } from 'react';
import { connect } from 'react-redux';
import '@/styles/creditAndLimit.scss';

import { formatNumber } from '@/utils';
import { getCredit } from '@/customerInvoices/selectors';
import I18n from '@/utils/I18n';

class CreditAndLimit extends Component {
  state = {
    creditApplied: false,
  };

  render() {
    return (
      <div className="credit_and_limit_wrap">
        {this.props.credit !== 0 ? (
          <div className="box">
            <h4 className="title">
              {I18n.get('cashflow.credit.drawer.title')}
            </h4>
            <span className="amount sum">
              {formatNumber(this.props.credit, true)}
            </span>

            {this.props.credit === 0 || this.state.creditApplied ? null : (
              <div className="applyForm">
                <a className="btn" onClick={() => this.applyForCredit()}>
                  {I18n.get('cashflow.credit.drawer.btn_apply')}
                </a>
              </div>
            )}

            {this.state.creditApplied === false ? null : (
              <div className="successForm">
                <p
                  dangerouslySetInnerHTML={{
                    __html: I18n.get('cashflow.credit.drawer.pending'),
                  }}
                />

                <a className="btn-second" onClick={() => this.resetCredit()}>
                  {I18n.get('cashflow.credit.drawer.btn_cancel')}
                </a>
              </div>
            )}
          </div>
        ) : null}
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    credit: getCredit(state),
    languageVariables: state.languageDropdown.variables,
  };
}

export default connect(mapStateToProps)(CreditAndLimit);
