import gql from 'graphql-tag';

export const fetchCashFlowGraph = gql`
	query CashFlow($startDate: Date!, $endDate: Date!, $size: String!) {
		cashflow(
			input: { startDate: $startDate, endDate: $endDate, size: $size }
		) {
			entries {
				incoming
				outgoing
				account
				type
				timeslice
			}
		}
	}
`;
