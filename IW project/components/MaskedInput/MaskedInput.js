import React from 'react';
import InputMask from 'react-input-mask';

const MaskedInput = ({
	id,
	name,
	placeholder,
	type,
	mask,
	required,
	onChange,
	onBlur,
	inputClass,
	labelText,
	labelClass,
	requiredInput,
	inputValue,
	disabledInput,
}) => {
	return (
		<div className="bx--form-item">
			<InputMask
				id={id}
				name={name}
				mask={mask}
				className={inputClass}
				placeholder={placeholder}
				autoComplete={name}
				type={type}
				value={inputValue}
				onChange={onChange}
				onBlur={onBlur}
				required={requiredInput}
				disabled={disabledInput}
			/>
			<label htmlFor={id} className={labelClass}>
				{labelText}
			</label>
		</div>
	);
};

export default MaskedInput;
