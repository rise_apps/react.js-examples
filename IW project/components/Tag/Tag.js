import React from 'react';
import PropTypes from 'prop-types';

// List of types
// ____________
// ibm
// beta
// third-party
// local
// dedicated
// custom
// experimental
// community
// private

const Tag = props => {
  return (
    <span className={`bx--tag bx--tag--${props.type} ${props.className}`}>
      {props.text}
    </span>
  );
};

Tag.defaultProps = {
  type: 'ibm'
};

Tag.propTypes = {
  type: PropTypes.string,
  className: PropTypes.string,
  text: PropTypes.string.isRequired
};

export default Tag;
