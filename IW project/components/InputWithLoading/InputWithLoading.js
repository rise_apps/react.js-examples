import React, { Fragment } from 'react';

import { Loading } from 'carbon-components-react';

const inputWithLoading = props => {
  return (
    <Fragment>
      {props.loading ? (
        <div className="input-loading">
          {props.children}
          <Loading withOverlay={false} small={true} />
        </div>
      ) : (
        props.children
      )}
    </Fragment>
  );
};

export default inputWithLoading;
