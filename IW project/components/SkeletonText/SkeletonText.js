import React from 'react';
import { SkeletonText } from 'carbon-components-react';

export const SkeletonTextBase = props => (
  <SkeletonText lineCount={1} className={'bx--skeleton-no-indent'} {...props} />
);
