import React from 'react';
import { DataTable } from 'carbon-components-react';
import {
	OverflowMenu,
	OverflowMenuItem,
	Search,
	Button,
	DataTableSkeleton,
	InlineNotification,
} from 'carbon-components-react';
import { getTag } from '@/shared/utility';

const {
	TableContainer,
	Table,
	TableHead,
	TableHeader,
	TableRow,
	TableBody,
	TableCell,
	TableToolbar,
} = DataTable;

// Inside of your component's `render` method
export default class DataTableBase extends React.Component {
	static defaultProps = {
		hasMenu: true,
	};

	onAccept = (e, id) => {
		e.stopPropagation();
		this.props.onAccept(id);
	};

	onDecline = (e, id) => {
		e.stopPropagation();
		this.props.onDecline(id);
	};

	renderTableLoading = (rows, headers, getHeaderProps) => {
		const { loading, tableClassName } = this.props;

		if (loading) {
			return <DataTableSkeleton zebra />;
		} else if (!rows.length) {
			return (
				<InlineNotification
					title={this.props.noDataTitle}
					subtitle=""
					hideCloseButton
					kind="info"
				/>
			);
		} else if (tableClassName) {
			return (
				<table className={tableClassName}>
					{this.renderTableContent(rows, headers, getHeaderProps)}
				</table>
			);
		} else {
			return (
				<Table>{this.renderTableContent(rows, headers, getHeaderProps)}</Table>
			);
		}
	};

	renderTableContent = (rows, headers, getHeaderProps) => (
		<React.Fragment>
			<TableHead>
				<TableRow>
					{headers.map(header => (
						<TableHeader {...getHeaderProps({ header })}>
							{header.header}
						</TableHeader>
					))}
					<TableHeader />
				</TableRow>
			</TableHead>
			<TableBody>
				{rows.map(row => (
					<TableRow key={row.id} onClick={() => this.props.onRowClick(row.id)}>
						{row.cells.map(cell => (
							<TableCell key={cell.id}>
								{cell.info.header === 'status' ||
								cell.info.header === 'plans' ? (
									getTag(cell.value)
								) : cell.info.header === 'email' ? (
									<a href={`mailto:${cell.value}`}>{cell.value}</a>
								) : (
									cell.value
								)}
							</TableCell>
						))}
						{this.props.approveAccess && (this.props.onAccept || this.props.onDecline) &&
						!this.props.hasMenu ? (
							<TableCell>
								<div className="bx--cell-actions">
									<a
										className="bx--accept"
										href="javascript:void(0);"
										onClick={e => this.onAccept(e, row.id)}
									>
										<svg width="16" height="16" viewBox="0 0 16 16">
											<path d="M8 16A8 8 0 1 1 8 0a8 8 0 0 1 0 16zm3.293-11.332L6.75 9.21 4.707 7.168 3.293 8.582 6.75 12.04l5.957-5.957-1.414-1.414z" />
										</svg>
									</a>
									<a
										className="bx--cancel"
										href="javascript:void(0);"
										onClick={e => this.onDecline(e, row.id)}
									>
										<svg width="16" height="16" viewBox="0 0 16 16">
											<path d="M8 6.586L5.879 4.464 4.464 5.88 6.586 8l-2.122 2.121 1.415 1.415L8 9.414l2.121 2.122 1.415-1.415L9.414 8l2.122-2.121-1.415-1.415L8 6.586zM8 16A8 8 0 1 1 8 0a8 8 0 0 1 0 16z" />
										</svg>
									</a>
								</div>
							</TableCell>
						) : null}
					</TableRow>
				))}
			</TableBody>
		</React.Fragment>
	);

	render() {
		const props = this.props;
		return (
			<DataTable
				{...props}
				rows={props.rows}
				headers={props.headers}
				render={({ rows, headers, getHeaderProps }) => (
					<TableContainer className={`fixOverflow ${props.containerClassName}`}>
						{!props.noSearch ? (
							<TableToolbar>
								<div
									className={`bx--search-panel ${!props.addNew &&
										'noPadding-right'}`}
								>
									<div className="bx--search" data-search role="search">
										<Search
											className="bx--search bx--search--sm"
											id="search__input-2"
											labelText="Search"
											placeHolderText={
												props.searchPlaceholder
													? props.searchPlaceholder
													: 'Search'
											}
											onChange={this.props.onInputChange}
										/>
									</div>
									{props.addNew && (
										<Button
											icon="add--solid"
											iconDescription="add"
											onClick={props.addNew}
											disabled={!props.accessDisable}
										>
											Add new
										</Button>
									)}
								</div>
							</TableToolbar>
						) : null}
						{this.renderTableLoading(rows, headers, getHeaderProps)}
					</TableContainer>
				)}
			/>
		);
	}
}
