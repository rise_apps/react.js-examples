import React, { Fragment } from 'react';
import Breadcrumbs from '@/components/Breadcrumbs';

const Page = props => {
  return (
    <Fragment>
      {props.hasBreadcrumbs ? (
        <div
          className={`bx--page-panel ${
            props.hasTitle ? 'bx--page-breadcrumb' : null
          }`}
        >
          <Breadcrumbs />
        </div>
      ) : null}
      {props.withoutWrap ? (
        props.children
      ) : (
        <div className="container">{props.children}</div>
      )}
    </Fragment>
  );
};

export default Page;
