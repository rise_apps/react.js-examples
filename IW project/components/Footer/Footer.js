import React from 'react';
import { Link } from 'react-router-dom';
import SocialList from '@/components/SocialList';

export default class Footer extends React.Component {
  render() {
    return (
      <footer className="footer">
        <div className="centered clearfix">
          <SocialList />
          <ul className="footer-list clearfix">
            <li>
              <Link to="/">Contact Us</Link>
            </li>
            <li>
              <Link to="/">Feedback</Link>
            </li>
            <li>
              <Link to="/">Terms &amp; Conditions</Link>
            </li>
            <li>
              <Link to="/">Privacy</Link>
            </li>
          </ul>
        </div>
      </footer>
    );
  }
}
