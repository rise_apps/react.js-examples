import { put, call } from 'redux-saga/effects';
import axios from 'axios';
import { generateList } from '@/shared/utility';

import * as actions from '@/store/actions/actionTypes';

export function* fetchWoundSummarySaga(action) {
	yield put({ type: actions.FETCH_WOUNDS_START });

	const url = `/wound-summary/${action.payload.woundId}/`;

	try {
		const { data } = yield axios.get(url);
		yield put({
			type: actions.FETCH_WOUND_SUMMARY_SUCCESS,
			payload: { results: data },
		});
	} catch (error) {
		yield put({
			type: actions.FETCH_WOUND_SUMMARY_FAIL,
			payload: { error },
		});
	}
}

export function* fetchWoundAssessmentsSaga(action) {
	yield put({ type: actions.FETCH_WOUNDS_START });

	const url = `/wound-assessment/${action.payload.woundId}/`;

	try {
		const { data } = yield axios.get(url);
		yield put({
			type: actions.FETCH_WOUND_ASSESSMENTS_SUCCESS,
			payload: { results: data.assessments },
		});
	} catch (error) {
		yield put({
			type: actions.FETCH_WOUND_ASSESSMENTS_FAIL,
			payload: { error },
		});
	}
}

export function* fetchWoundPlanOfCareSaga(action) {
	yield put({ type: actions.FETCH_WOUNDS_START });

	const url = `/wounds/${action.payload.woundId}/carePlan/`;

	try {
		const { data } = yield axios.get(url);
		yield put({
			type: actions.FETCH_WOUND_PLAN_OF_CARE_SUCCESS,
			payload: { results: data },
		});
	} catch (error) {
		yield put({
			type: actions.FETCH_WOUND_PLAN_OF_CARE_FAIL,
			payload: { error },
		});
	}
}

export function* fetchWoundTreatmentsSaga(action) {
	yield put({ type: actions.FETCH_WOUNDS_START });

	const url = `/wounds/${action.payload.woundId}/treatments/`;

	try {
		const { data } = yield axios.get(url);
		yield put({
			type: actions.FETCH_WOUND_TREATMENTS_SUCCESS,
			payload: { results: data },
		});
	} catch (error) {
		yield put({
			type: actions.FETCH_WOUND_TREATMENTS_FAIL,
			payload: { error },
		});
	}
}

export function* fetchLocationsAndEtiologiesSaga(action) {
	yield put({ type: actions.FETCH_WOUNDS_START });

	const url = `/get-catalogs/`;

	try {
		const { data } = yield axios.get(url);
		const etiologies = yield call(generateList, data.etiologies, 'etiology');
		const locations = yield call(generateList, data.locations, 'location');

		yield put({
			type: actions.FETCH_LOCATIONS_AND_ETIOLOGIES_SUCCESS,
			payload: { etiologies, locations },
		});
	} catch (error) {
		yield put({
			type: actions.FETCH_WOUNDS_FAIL,
			payload: { error },
		});
	}
}

export function* approveWoundPlanOfCareSaga(action) {
	const { woundId, carePlanId } = action.payload;
	const url = `/wounds/${woundId}/carePlan/${carePlanId}/approve/`;

	try {
		const { data } = yield axios.post(url);
		yield put({
			type: actions.APPROVE_WOUND_PLAN_SUCCESS,
			payload: { results: data },
		});
	} catch (error) {
		yield put({
			type: actions.APPROVE_WOUND_PLAN_FAIL,
			payload: { error },
		});
	}
}

export function* approveWoundAssessmentSaga(action) {
	const { woundId, assessmentId } = action.payload;
	const url = `/wounds/${woundId}/assessment/${assessmentId}/approve/`;

	try {
		const { data } = yield axios.post(url);
		yield put({
			type: actions.APPROVE_WOUND_ASSESSMENT_SUCCESS,
			payload: { results: data },
		});
	} catch (error) {
		yield put({
			type: actions.APPROVE_WOUND_ASSESSMENT_FAIL,
			payload: { error },
		});
	}
}

export function* fetchWoundSummaryEditSaga(action) {
	const { woundId } = action.payload;
	const url = `/wound-summary-update/${woundId}/`;

	try {
		const { data } = yield axios.get(url);

		yield put({
			type: actions.FETCH_WOUND_SUMMARY_EDIT_SUCCESS,
			payload: { results: data },
		});
	} catch (error) {
		yield put({
			type: actions.FETCH_WOUND_SUMMARY_EDIT_FAIL,
			payload: { error },
		});
	}
}

export function* woundSummaryEditSaga(action) {
	const { woundId, locationId, etiologyId } = action.payload;
	const url = `/wound-summary-update/${woundId}/`;

	try {
		yield axios.post(url, {
			locationId,
			etiologyId,
			view: 'front',
		});

		yield put({
			type: actions.WOUND_SUMMARY_EDIT_SUCCESS,
		});
	} catch (error) {
		yield put({
			type: actions.WOUND_SUMMARY_EDIT_FAIL,
			payload: { error },
		});
	}
}
