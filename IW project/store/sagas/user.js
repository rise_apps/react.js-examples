import { put } from 'redux-saga/effects';
import axios from 'axios';

import * as actions from '@/store/actions/actionTypes';

export function* fetchUserDataSaga(action) {
	yield put({ type: actions.FETCH_USER_START });

	const url = `/userProfile/${action.payload.userId}/`;

	try {
		const { data } = yield axios.get(url);
		yield put({
			type: actions.FETCH_USER_SUCCESS,
			payload: { userData: data },
		});
	} catch (error) {
		yield put({
			type: actions.FETCH_USER_FAIL,
			payload: { error },
		});
	}
}
