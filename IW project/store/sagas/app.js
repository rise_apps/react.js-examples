import { put, call } from 'redux-saga/effects';
import * as actions from '../actions/actionTypes';
import { fetchUserDataSaga } from './user';
import { fetchPatientsListSaga } from './patients';
import { LoadableApp } from '@/Layout';

export function* appDataLoadingSaga(action) {
	yield call(fetchUserDataSaga, { payload: action.payload });
	yield call(fetchPatientsListSaga, { payload: { limit: 10, offset: 0 } });
	yield LoadableApp.preload();
	yield put({ type: actions.APP_LOADED });
}
