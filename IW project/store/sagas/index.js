import { takeEvery, call } from 'redux-saga/effects';

import * as actionTypes from '../actions/actionTypes';
import {
	authUserSaga,
	logoutSaga,
	changePassSaga,
	authCheckSessionStorageSaga,
	resetPass,
} from './auth';
import { fetchUserDataSaga } from './user';
import { appDataLoadingSaga } from './app';
import {
	fetchPatientsListSaga,
	fetchPatientDataSaga,
	addNewPatientSaga,
	fetchReportDataSaga,
	fetchPlanDataSaga,
	getWoundUserDimensionDataSaga,
	editWoundUserDimensionDataSaga,
	getWoundTunnelsDataSaga,
	fetchPatientSuppliesListSaga,
	fetchPatientShopListSaga,
	fetchPatientWoundsListSaga,
} from './patients';
import {
	fetchPrimaryPhysicianSaga,
	fetchPhysicianApprovalSaga,
	physicianDeclineSaga,
	physicianApproveSaga,
} from './physician';
import {
	fetchSupplyHistoryListSaga,
	fetchSupplyHistoryDataSaga,
	fetchSupplyShopListSaga,
	fetchSupplyShopDataSaga,
	declineOrPlaceOrderShopOrderSaga,
} from './supplyChain';
import {
	fetchSettingsUsersListSaga,
	fetchSettingsDeviceListSaga,
	fetchOrganizationDataSaga,
	fetchSettingsPrimaryPhysicianListSaga,
	addNewUserSaga,
} from './settings';
import {
	fetchWoundSummarySaga,
	fetchWoundPlanOfCareSaga,
	fetchLocationsAndEtiologiesSaga,
	fetchWoundAssessmentsSaga,
	fetchWoundTreatmentsSaga,
	approveWoundPlanOfCareSaga,
	approveWoundAssessmentSaga,
	woundSummaryEditSaga,
	fetchWoundSummaryEditSaga,
} from './wounds';
import { fetchTreatmentsAlgorithmSaga } from './treatmentsAlgorithm';
import {
	fetchSupplyCatalogSaga,
	fetchSupplyCategoriesSaga,
} from './supplyCatalog';
import { addNewPlanOfCareSaga } from './planOfCare';

export function* rootSaga() {
	//Auth
	yield takeEvery(actionTypes.AUTH_USER, authUserSaga);
	yield takeEvery(actionTypes.AUTH_INITIATE_LOGOUT, logoutSaga);
	yield takeEvery(actionTypes.AUTH_CHANGE_PASS, changePassSaga);
	yield takeEvery(actionTypes.AUTH_RESET_PASS, resetPass);
	yield takeEvery(
		actionTypes.AUTH_CHECK_INITIAL_STATE,
		authCheckSessionStorageSaga,
	);

	//User
	yield takeEvery(actionTypes.FETCH_USER, fetchUserDataSaga);

	//App
	yield takeEvery(actionTypes.APP_LOADING, appDataLoadingSaga);

	//Patients
	yield takeEvery(actionTypes.FETCH_PATIENTS_LIST, fetchPatientsListSaga);
	yield takeEvery(actionTypes.FETCH_PATIENT_DATA, fetchPatientDataSaga);
	yield takeEvery(actionTypes.ADD_NEW_PATIENT, addNewPatientSaga);
	yield takeEvery(actionTypes.APP_LOADING, appDataLoadingSaga);
	yield takeEvery(actionTypes.FETCH_PATIENT_REPORT, fetchReportDataSaga);
	yield takeEvery(actionTypes.FETCH_PATIENT_PLAN, fetchPlanDataSaga);
	yield takeEvery(
		actionTypes.GET_WOUND_USER_DIMENSION,
		getWoundUserDimensionDataSaga,
	);
	yield takeEvery(
		actionTypes.EDIT_WOUND_USER_DIMENSION_DATA,
		editWoundUserDimensionDataSaga,
	);
    yield takeEvery(actionTypes.GET_WOUND_TUNNELS, getWoundTunnelsDataSaga);
    yield takeEvery(
		actionTypes.FETCH_PATIENT_SUPPLIES_LIST,
		fetchPatientSuppliesListSaga,
	);
	yield takeEvery(
		actionTypes.FETCH_PATIENT_SHOP_LIST,
		fetchPatientShopListSaga,
	);
	yield takeEvery(
		actionTypes.FETCH_PATIENT_WOUNDS_LIST,
		fetchPatientWoundsListSaga,
	);

	//Supply Chain
	yield takeEvery(
		actionTypes.FETCH_SUPPLY_HISTORY_LIST,
		fetchSupplyHistoryListSaga,
	);
	yield takeEvery(
		actionTypes.FETCH_SUPPLY_HISTORY_DATA,
		fetchSupplyHistoryDataSaga,
	);
	yield takeEvery(actionTypes.FETCH_SUPPLY_SHOP_LIST, fetchSupplyShopListSaga);
	yield takeEvery(actionTypes.FETCH_SUPPLY_SHOP_DATA, fetchSupplyShopDataSaga);

	yield takeEvery(
		actionTypes.DECLINE_OR_PLACE_ORDER_SHOP_ORDER,
		declineOrPlaceOrderShopOrderSaga,
	);

	//Settings
	yield takeEvery(
		actionTypes.FETCH_SETTINGS_USERS_LIST,
		fetchSettingsUsersListSaga,
	);
	yield takeEvery(
		actionTypes.FETCH_SETTINGS_DEVICE_LIST,
		fetchSettingsDeviceListSaga,
	);
	yield takeEvery(
		actionTypes.FETCH_SETTINGS_ORGANIZATION_DATA,
		fetchOrganizationDataSaga,
	);
	yield takeEvery(
		actionTypes.FETCH_SETTINGS_PRIMARY_PHYSICIANS_LIST,
		fetchSettingsPrimaryPhysicianListSaga,
	);
	yield takeEvery(actionTypes.ADD_NEW_USER, addNewUserSaga);

	//Physician
	yield takeEvery(
		actionTypes.FETCH_PRIMARY_PHYSICIAN,
		fetchPrimaryPhysicianSaga,
	);
	yield takeEvery(
		actionTypes.FETCH_PHYSICIAN_APPROVAL,
		fetchPhysicianApprovalSaga,
	);
	yield takeEvery(actionTypes.PHYSICIAN_DECLINE, physicianDeclineSaga);
	yield takeEvery(actionTypes.PHYSICIAN_APPROVE, physicianApproveSaga);

	//Wounds
	yield takeEvery(
		actionTypes.FETCH_LOCATIONS_AND_ETIOLOGIES,
		fetchLocationsAndEtiologiesSaga,
	);
	yield takeEvery(actionTypes.FETCH_WOUND_SUMMARY, fetchWoundSummarySaga);
	yield takeEvery(
		actionTypes.FETCH_WOUND_PLAN_OF_CARE,
		fetchWoundPlanOfCareSaga,
	);
	yield takeEvery(
		actionTypes.FETCH_WOUND_ASSESSMENTS,
		fetchWoundAssessmentsSaga,
	);
	yield takeEvery(actionTypes.FETCH_WOUND_TREATMENTS, fetchWoundTreatmentsSaga);
	yield takeEvery(actionTypes.APPROVE_WOUND_PLAN, approveWoundPlanOfCareSaga);
	yield takeEvery(
		actionTypes.APPROVE_WOUND_ASSESSMENT,
		approveWoundAssessmentSaga,
	);
	yield takeEvery(
		actionTypes.FETCH_WOUND_SUMMARY_EDIT,
		fetchWoundSummaryEditSaga,
	);
	yield takeEvery(actionTypes.WOUND_SUMMARY_EDIT, woundSummaryEditSaga);

	// Treatments Algorithm
	yield takeEvery(
		actionTypes.FETCH_TREATMENTS_ALGORITHM,
		fetchTreatmentsAlgorithmSaga,
	);

	// Supply Catalog
	yield takeEvery(actionTypes.FETCH_SUPPLY_CATALOG, fetchSupplyCatalogSaga);
	yield takeEvery(
		actionTypes.FETCH_SUPPLY_CATEGORIES,
		fetchSupplyCategoriesSaga,
	);

	// Plan of Care
	yield takeEvery(actionTypes.ADD_NEW_PLAN_OF_CARE, addNewPlanOfCareSaga);

	yield call(authCheckSessionStorageSaga);
}
