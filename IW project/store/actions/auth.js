import * as actionTypes from './actionTypes';

export const auth = (email, password) => {
  return dispatch => {
    dispatch({
      type: actionTypes.AUTH_USER,
      payload: {
        email,
        password
      }
    });
  };
};

export const logout = () => {
  return dispatch => {
    dispatch({
      type: actionTypes.AUTH_INITIATE_LOGOUT
    });
  };
};

export const changePass = newPassword => {
  return dispatch => {
    dispatch({
      type: actionTypes.AUTH_CHANGE_PASS,
      payload: {
        newPassword
      }
    });
  };
};

export const resetPass = email => {
  return dispatch => {
    dispatch({
      type: actionTypes.AUTH_RESET_PASS,
      payload: {
        email
      }
    });
  };
};

export const clearChangePassStatus = () => {
  return dispatch => {
    dispatch({
      type: actionTypes.AUTH_CHANGE_PASS_CLEAR_STATUS
    });
  };
};
