import * as actionTypes from '@/store/actions/actionTypes';

export const fetchPatientsList = (limit, offset, search) => dispatch => {
	dispatch({
		type: actionTypes.FETCH_PATIENTS_LIST,
		payload: {
			limit,
			offset,
			search,
		},
	});
};

export const fetchPatientData = patientId => dispatch => {
	dispatch({
		type: actionTypes.FETCH_PATIENT_DATA,
		payload: {
			patientId,
		},
	});
};

export const addNewPatient = patientData => dispatch => {
	dispatch({
		type: actionTypes.ADD_NEW_PATIENT,
		payload: patientData,
	});
};

export const clearNewPatientStatus = () => dispatch => {
	dispatch({
		type: actionTypes.ADD_NEW_PATIENT_CLEAR_STATUS,
	});
};

export const fetchReportData = reportId => dispatch => {
	dispatch({
		type: actionTypes.FETCH_PATIENT_REPORT,
		payload: {
			reportId,
		},
	});
};

export const fetchPlanData = reportId => dispatch => {
	dispatch({
		type: actionTypes.FETCH_PATIENT_PLAN,
		payload: {
			reportId,
		},
	});
};

export const getWoundUserDimensionData = assessmentId => dispatch => {
	dispatch({
		type: actionTypes.GET_WOUND_USER_DIMENSION,
		payload: {
			assessmentId,
		},
	});
};

export const editWoundUserDimensionData = ({
	assessmentId,
	woundData,
}) => dispatch => {
	dispatch({
		type: actionTypes.EDIT_WOUND_USER_DIMENSION_DATA,
		payload: {
			assessmentId,
			woundData,
		},
	});
};

export const getWoundTunnelsData = assessmentId => dispatch => {
    dispatch({
        type: actionTypes.GET_WOUND_TUNNELS,
        payload: {
            assessmentId,
        },
    });
};

export const clearPatientWoundMeasurementDataStatus = () => dispatch => {
	dispatch({
		type: actionTypes.CLEAR_PATIENT_WOUND_MEASUREMENT_DATA_STATUS,
	});
};

export const fetchPatientSuppliesList = data => dispatch => {
	dispatch({
		type: actionTypes.FETCH_PATIENT_SUPPLIES_LIST,
		payload: {
			patient: data.patient,
			search: data.search,
			limit: data.limit,
			offset: data.offset,
		},
	});
};

export const fetchPatientShopList = data => dispatch => {
	dispatch({
		type: actionTypes.FETCH_PATIENT_SHOP_LIST,
		payload: {
			patient: data.patient,
			search: data.search,
			limit: data.limit,
			offset: data.offset,
		},
	});
};

export const fetchPatientWoundsList = data => dispatch => {
	dispatch({
		type: actionTypes.FETCH_PATIENT_WOUNDS_LIST,
		payload: {
			patient: data.patient,
			limit: data.limit,
			offset: data.offset,
		},
	});
};
