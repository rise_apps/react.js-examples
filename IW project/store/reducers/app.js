import * as actionTypes from '@/store/actions/actionTypes';

const initialState = {
  loading: false,
  loaded: false,
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.APP_LOADING:
      return { ...state, loading: true, loaded: false };
    case actionTypes.APP_LOADED:
      return { ...state, loading: false, loaded: true };
    case actionTypes.APP_CLEAR_DATA:
      return { ...state, loading: false, loaded: false };
    default:
      return state;
  }
};

export default reducer;
