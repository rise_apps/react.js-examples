import * as actionTypes from '@/store/actions/actionTypes';

const initialState = {
	patientsList: [],
	suppliesList: [],
	shoppingOrders: [],
	wounds: [],
	patient: {},
	primaryPhysician: {},
	report: {},
	plan: {},
	loading: false,
	error: null,
	count: 0,
	patientId: null,
	woundEdited: null,
	measurement: {},
	tunnelsData: {},
	userDimension: {},
};

const reducer = (state = initialState, action) => {
	switch (action.type) {
		case actionTypes.PATIENTS_LOADING_START:
			return {
				...state,
				loading: true,
			};
		case actionTypes.FETCH_PATIENTS_START:
			return {
				...state,
				loading: true,
				report: {},
				plan: {},
			};
		case actionTypes.FETCH_PATIENTS_FAIL:
			return { ...state, loading: false, error: action.payload.error };
		case actionTypes.FETCH_PATIENTS_LIST_SUCCESS:
			return {
				...state,
				loading: false,
				patientsList: action.payload.patientsList,
				count: action.payload.count,
			};
		case actionTypes.FETCH_PATIENT_DATA_SUCCESS:
			return {
				...state,
				loading: false,
				patient: action.payload.patient,
			};
		case actionTypes.ADD_NEW_PATIENT_SUCCESS:
			return {
				...state,
				patientId: action.payload.patientId,
			};
		case actionTypes.ADD_NEW_PATIENT_FAIL:
			return {
				...state,
				error: action.payload.error,
			};
		case actionTypes.ADD_NEW_PATIENT_CLEAR_STATUS:
			return {
				...state,
				error: null,
				patientId: null,
			};
		case actionTypes.FETCH_PATIENT_REPORT_START:
			return { ...state, loading: true };
		case actionTypes.FETCH_PATIENT_REPORT_SUCCESS:
			return {
				...state,
				loading: false,
				report: action.payload.report,
			};
		case actionTypes.FETCH_PATIENT_REPORT_FAIL:
			return { ...state, loading: false, error: action.payload.error };
		case actionTypes.FETCH_PATIENT_PLAN_START:
			return { ...state, loading: true };
		case actionTypes.FETCH_PATIENT_PLAN_SUCCESS:
			return {
				...state,
				loading: false,
				plan: action.payload.plan,
			};
		case actionTypes.FETCH_PATIENT_PLAN_FAIL:
			return { ...state, loading: false, error: action.payload.error };
		case actionTypes.GET_WOUND_USER_DIMENSION_SUCCESS:
			return {
				...state,
				loading: false,
				error: null,
				userDimension: action.payload.userDimension,
			};
		case actionTypes.GET_WOUND_USER_DIMENSION_FAIL:
			return { ...state, loading: false, error: action.payload.error };
		case actionTypes.GET_WOUND_TUNNELS_SUCCESS:
			return {
				...state,
				loading: false,
				error: null,
				tunnelsData: action.payload.tunnelsData,
			};
		case actionTypes.GET_WOUND_TUNNELS_FAIL:
			return { ...state, loading: false, error: action.payload.error };
		case actionTypes.EDIT_WOUND_USER_DIMENSION_DATA_START:
			return { ...state, loading: true };
		case actionTypes.EDIT_WOUND_USER_DIMENSION_DATA_SUCCESS:
			return {
				...state,
				loading: false,
				error: null,
				woundEdited: action.payload.woundEdited,
			};
		case actionTypes.EDIT_WOUND_USER_DIMENSION_DATA_FAIL:
			return { ...state, loading: false, error: action.payload.error };
		case actionTypes.CLEAR_PATIENT_WOUND_MEASUREMENT_DATA_STATUS:
			return { ...state, error: null, woundEdited: null };
		case actionTypes.FETCH_PATIENT_SUPPLIES_LIST_FAIL:
			return {
				...state,
				loading: false,
				error: action.payload.error,
				suppliesList: [],
				count: null,
			};
		case actionTypes.FETCH_PATIENT_SUPPLIES_LIST_SUCCESS:
			return {
				...state,
				loading: false,
				suppliesList: action.payload.suppliesList,
				error: {},
				count: action.payload.count,
			};
		case actionTypes.FETCH_PATIENT_SHOP_LIST_FAIL:
			return {
				...state,
				loading: false,
				error: action.payload.error,
				shoppingOrders: [],
				count: null,
			};
		case actionTypes.FETCH_PATIENT_SHOP_LIST_SUCCESS:
			return {
				...state,
				loading: false,
				shoppingOrders: action.payload.shoppingOrders,
				error: {},
				count: action.payload.count,
			};
		case actionTypes.FETCH_PATIENT_WOUNDS_LIST_FAIL:
			return {
				...state,
				loading: false,
				error: action.payload.error,
				wounds: [],
				count: null,
			};
		case actionTypes.FETCH_PATIENT_WOUNDS_LIST_SUCCESS:
			return {
				...state,
				loading: false,
				wounds: action.payload.wounds,
				error: {},
				count: action.payload.count,
			};
		default:
			return state;
	}
};

export default reducer;
