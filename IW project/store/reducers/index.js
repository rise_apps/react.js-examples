import { combineReducers } from 'redux';
import authReducer from './auth';
import userReducer from './user';
import patientsReducer from './patients';
import appReducer from './app';
import supplyChainReducer from './supplyChain';
import physicianReducer from './physician';
import settingsReducer from './settings';
import woundsReducer from './wounds';
import treatmentsAlgorithmReducer from './treatmentsAlgorithm';
import supplyCatalogReducer from './supplyCatalog';
import planOfCareReducer from './planOfCare';

const rootReducer = combineReducers({
	auth: authReducer,
	user: userReducer,
	patients: patientsReducer,
	app: appReducer,
	supplyChain: supplyChainReducer,
	physician: physicianReducer,
	settings: settingsReducer,
	wounds: woundsReducer,
	treatmentsAlgorithm: treatmentsAlgorithmReducer,
	supplyCatalog: supplyCatalogReducer,
	planOfCare: planOfCareReducer,
});

export default rootReducer;
