import * as actionTypes from '../actions/actionTypes';

const initialState = {
	token: null,
	userId: null,
	error: null,
	loading: false,
	resetPassSuccess: false,
	changePassSuccess: false,
};

const reducer = (state = initialState, action) => {
	switch (action.type) {
		case actionTypes.AUTH_START:
			return { ...state, loading: true, error: null };
		case actionTypes.AUTH_SUCCESS:
			return {
				...state,
				token: action.payload.idToken,
				userId: action.payload.userId,
                userRoles: action.payload.userRoles,
				organizationId: action.payload.organizationId,
				error: null,
				loading: false,
			};
		case actionTypes.AUTH_FAIL:
			return {
				...state,
				error: 'Login or Password incorrect!',
				loading: false,
			};
		case actionTypes.AUTH_LOGOUT:
			return {
				...state,
				token: null,
				userId: null,
			};
		case actionTypes.AUTH_CHANGE_PASS_SUCCESS:
			return {
				...state,
				changePassSuccess: true,
				error: null,
			};
		case actionTypes.AUTH_CHANGE_PASS_FAIL:
			return state;
		case actionTypes.AUTH_RESET_PASS:
			return { ...state, loading: true };
		case actionTypes.AUTH_RESET_PASS_SUCCESS:
			return { ...state, resetPassSuccess: true, error: null };
		case actionTypes.AUTH_RESET_PASS_FAIL:
			return {
				...state,
				resetPassSuccess: false,
				error: action.payload.error,
			};
		case actionTypes.AUTH_CHANGE_PASS_CLEAR_STATUS:
			return { ...state, changePassSuccess: false };
		default:
			return state;
	}
};

export default reducer;
