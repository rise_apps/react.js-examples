// Simple example in one file
import { takeEvery, call, put } from 'redux-saga/effects';
import * as actionTypes from '@app/actions/actionTypes';

// Root saga is connected in applyMiddleware
export function* rootSaga() {
  // Spawns a saga on each action dispatched to the Store that matches pattern AUTH_USER
  yield takeEvery(actionTypes.AUTH_USER, authUserSaga);
}

function* authUserSaga(action) {
  // Creates an Effect description that instructs the middleware to dispatch an action to the Store
  yield put({ type: actions.AUTH_START });

  try {
    // Send post request to the server
    const { data } = yield axios.post('/auth/web-login/', {
      username: action.payload.email,
      password: action.payload.password,
    });

    // Call function for set authorization token to session storage and axios default headers
    yield call(setAuthorizationToken, data.token);
    // Put auth data to the Store
    yield put({
      type: actions.AUTH_SUCCESS,
      payload: {
        idToken: data.token,
        userId: data.userId,
        userRoles: data.userRoles,
        organizationId: data.organizationId,
      },
    });
  } catch (error) {
    // Put error data to the Store
    yield put({
      type: actions.AUTH_FAIL,
      payload: { error },
    });
  }
}
