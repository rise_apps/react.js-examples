import io from 'socket.io-client';
import { call, put, take, fork, race, cancel, all } from 'redux-saga/effects';

// Socket task manager for closing socket connection and canceling forked function
export function* socketTaskManager() {
    while (true) {
        yield take('WEBSOCKET_START_TASK');
        // Call function in background
        const task = yield fork(connectSocket);
        // Start socket connection task
        yield put({
            type: 'WEBSOCKET_CONNECT',
            payload: { uri: socketServerURL, task },
        });
    }
}

function* connectSocket() {
    while (true) {
        const { payload } = yield take('WEBSOCKET_CONNECT');
        try {
            // Create connect to socket.io
            const socket = io(payload.uri);
            // Create a chanel listener for incoming data from socket.io
            const channel = yield call(initSocketListener, socket);
            // Create a race task for sending internal data to Store and cancel the task
            yield race({
                task: all([call(externalListener, channel, payload.task)]),
            });
        } catch (e) {
            console.warn(e);
        }
    }
}

function* externalListener(chanel, task) {
  while (true) {
    const action = yield take(chanel);
    // Put incoming data to the store
    yield put(action);
    // Close the task
    yield cancel(task);
  }
}

function initSocketListener(socket) {
  return eventChannel(emit => {
    // Socket.io event listener and emit data to race externalListener
    socket.on('card-id', data => {
      emit({ type: AUTH_WAIT_CARD_SUCCESS, payload: data });
    });

    return () => {
      socket.close();
    };
  });
}
